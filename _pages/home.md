---
layout: splash
permalink: /
hidden: true
header:
  overlay_color: "#5e616c"
  overlay_image: /assets/images/Header-Image.png
excerpt: Need help with Open Science? Get it here!
feature_row:
  - image_path: /assets/images/Verena-Heise.jpeg
    alt: "Verena"
    title: "Verena Heise"
    excerpt: "Freelance Open Science Trainer/Consultant."
    url: "https://vheise.com/"
    btn_class: "btn--primary"
    btn_label: "Homepage"
  - image_path: /assets/images/Heidi-Seibold.png
    alt: "Heidi"
    title: "Heidi Seibold"
    excerpt: "Trainer for Open and Reproducible (Data) Science."
    url: "https://heidiseibold.gitlab.io/"
    btn_class: "btn--primary"
    btn_label: "Homepage"
---

# Our experts

{% include feature_row %}
